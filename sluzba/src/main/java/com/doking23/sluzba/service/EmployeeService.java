package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.*;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.EvaluationNotFoundException;
import com.doking23.sluzba.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Transactional
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final CityRepository cityRepository;
	private final RoleRepository roleRepository;
	private final TaskRepository taskRepository;
	private final SchoolRepository schoolRepository;
	private final EvaluationRepository evaluationRepository;

	public List<Employee> getAllEmployees(){
		return employeeRepository.findAll();
	}
	public Employee getEmployeeById(long employeeId)throws EmployeeNotFoundException{
		if(employeeRepository.findById(employeeId).isPresent()){
			return employeeRepository.getOne(employeeId);
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
	@Transactional
	public void createEmployee(Employee employee) {

		City employeeCity = employee.getCity();
		School employeeSchool = employee.getSchool();
		City existingCity = cityRepository.findCityByPttEquals(employeeCity.getPtt());
		School existingSchool = schoolRepository.findSchoolBySchoolNameEqualsAndSchoolTypeEquals(employeeSchool.getSchoolName()
				,employeeSchool.getSchoolType());
		if(existingCity != null && existingSchool !=null){
			employee.setCity(existingCity);
			employee.setSchool(existingSchool);
			employeeRepository.save(employee);
		}
	}
	public void deleteEmployee(long id){
		employeeRepository.deleteById(id);
	}
	public void evaluateEmployee(long employeeId, Evaluation evaluation){
		if(employeeRepository.findById(employeeId).isPresent()){
			Employee employeeToUpdate = employeeRepository.findById(employeeId).get();
			employeeToUpdate.setEvaluation(evaluation);
			employeeRepository.save(employeeToUpdate);
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
	public void changeCity(long employeeId, City city) {
		if(employeeRepository.findById(employeeId).isPresent()){
			Employee employeeToUpdate = employeeRepository.findById(employeeId).get();
			employeeToUpdate.setCity(city);
			employeeRepository.save(employeeToUpdate);
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
	public void addRolesToEmployee(long employeeId, List<Role> roles){
		if(employeeRepository.findById(employeeId).isPresent()){
			Employee employeeToUpdate = employeeRepository.findById(employeeId).get();
			roles.stream().filter(Objects::nonNull).forEach(employeeToUpdate::addRole);
			employeeRepository.save(employeeToUpdate);
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
	public void addTasksToEmployee(long employeeId, List<Task> tasks) {
		if(employeeRepository.findById(employeeId).isPresent()){
			Employee employeeToUpdate = employeeRepository.findById(employeeId).get();
			tasks.stream().filter(Objects::nonNull).forEach(employeeToUpdate::addTask);
			employeeRepository.save(employeeToUpdate);
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
	public void removeEvaluation(long employeeId,long evaluationId){
		if(employeeRepository.findById(employeeId).isPresent()){
			Employee employeeToUpdate = employeeRepository.findById(employeeId).get();
			if(roleRepository.findById(evaluationId).isPresent()){
				employeeToUpdate.removeRole(roleRepository.findById(evaluationId).get());
				employeeRepository.save(employeeToUpdate);
			}else throw new EvaluationNotFoundException("Role with ID: "+evaluationId+" not found");
		}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
	}
}
