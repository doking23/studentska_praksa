package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Role;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.RoleNotFoundException;
import com.doking23.sluzba.repository.EmployeeRepository;
import com.doking23.sluzba.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RoleService {
	private final RoleRepository roleRepository;
	private final EmployeeRepository employeeRepository;
	public List<Role> getAllRoles(){
		return roleRepository.findAll();
	}
	public Role getRoleById(long id )throws RoleNotFoundException{
		if(roleRepository.findById(id).isPresent()){
			return roleRepository.getOne(id);
		}else throw new RoleNotFoundException("Role with ID: " + id + " not found");
	}
	public void addRole(Role role){
		roleRepository.save(role);
	}
	public void updateRole(long id,Role role)throws RoleNotFoundException{
		if(roleRepository.findById(id).isPresent()){
			roleRepository.save(role);
		}else throw new RoleNotFoundException("Role with ID: " + id + " not found");
	}
	public void deleteRole(long id){
		if(roleRepository.findById(id).isPresent()){
			roleRepository.deleteById(id);
		}else throw new RoleNotFoundException("Role with ID: " + id + " not found");
	}
	public void addRoleToEmployee(long employeeId,long roleId)throws RoleNotFoundException,EmployeeNotFoundException{
		if(roleRepository.findById(roleId).isPresent()){
			Role roleToAdd = roleRepository.getOne(roleId);
			if(employeeRepository.findById(employeeId).isPresent()){
				Employee employeeToUpdate = employeeRepository.getOne(employeeId);
				employeeToUpdate.addRole(roleToAdd);
				employeeRepository.save(employeeToUpdate);
			}else throw new EmployeeNotFoundException("Employee with ID: " + employeeId + " not found");
		}else throw new RoleNotFoundException("Role with ID: " + roleId + " not found");
	}
	public void removeRoleFromEmployee(long employeeId,long roleId)throws RoleNotFoundException,EmployeeNotFoundException{
		if(roleRepository.findById(roleId).isPresent()){
			Role roleToDelete = roleRepository.getOne(roleId);
			if(employeeRepository.findById(employeeId).isPresent()){
				Employee employeeToUpdate = employeeRepository.getOne(employeeId);
				employeeToUpdate.removeRole(roleToDelete);
				employeeRepository.save(employeeToUpdate);
			}else throw new EmployeeNotFoundException("Employee with ID: " + employeeId + " not found");
		}else throw new RoleNotFoundException("Role with ID: " + roleId + " not found");
	}
}
