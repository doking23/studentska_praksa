package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Task;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.RoleNotFoundException;
import com.doking23.sluzba.exceptions.TaskNotFoundException;
import com.doking23.sluzba.repository.EmployeeRepository;
import com.doking23.sluzba.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TaskService {
	private final TaskRepository taskRepository;
	private final EmployeeRepository employeeRepository;
	public Task getTaskById(long id){
		return taskRepository.getOne(id);
	}
	public List<Task> getAllTasks(){
		return taskRepository.findAll();
	}
	public void createTask(Task task){
		taskRepository.save(task);
	}
	public void deleteTask(long id) throws TaskNotFoundException{
		if(taskRepository.findById(id).isPresent()){
			taskRepository.deleteById(id);
		}else throw new TaskNotFoundException("Task with ID: " +id+" not found");
	}
	public void addTaskToEmployee(long employeeId,long taskId)throws TaskNotFoundException,EmployeeNotFoundException{
		if(taskRepository.findById(taskId).isPresent()){
			Task taskToAddToEmployee = taskRepository.getOne(taskId);
			if(employeeRepository.findById(employeeId).isPresent()){
				Employee employeeToUpdate = employeeRepository.getOne(employeeId);
				taskToAddToEmployee.addEmployee(employeeToUpdate);
				employeeToUpdate.addTask(taskToAddToEmployee);
				employeeRepository.save(employeeToUpdate);
			}else throw new EmployeeNotFoundException("Employee with ID: " + employeeId + " not found");
		}else throw new TaskNotFoundException("Task with ID: " +taskId+" not found");
	}
	public void removeTaskFromEmployee(long taskId,long employeeId)throws TaskNotFoundException,EmployeeNotFoundException{
		if(taskRepository.findById(taskId).isPresent()){
			Task taskToRemove = taskRepository.getOne(taskId);
			if(employeeRepository.findById(employeeId).isPresent()){
				Employee employeeToUpdate = employeeRepository.getOne(employeeId);
				employeeToUpdate.removeTask(taskToRemove);
				employeeRepository.save(employeeToUpdate);
			}else throw new EmployeeNotFoundException("Employee with ID: " + employeeId + " not found");
		}else throw new TaskNotFoundException("Task with ID: " +taskId+" not found");
	}
}
