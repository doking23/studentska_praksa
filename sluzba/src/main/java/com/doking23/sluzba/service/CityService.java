package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.exceptions.CityNotFoundException;
import com.doking23.sluzba.repository.CityRepository;
import com.doking23.sluzba.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CityService {
	private final CityRepository cityRepository;
	private final EmployeeRepository employeeRepository;

	public void createCity(City city) {
		cityRepository.save(city);
	}
	public void deleteCity(long id) throws CityNotFoundException{
		if(cityRepository.findById(id).isPresent()){
			cityRepository.deleteById(id);
		}else throw new CityNotFoundException("City with ID: " + id + " not found");
	}
	public void updateCity(long id,City city) throws CityNotFoundException{
		if(cityRepository.findById(id).isPresent()){
			City cityToUpdate = cityRepository.findById(id).get();
			cityToUpdate.setCityName(city.getCityName());
			cityToUpdate.setPtt(city.getPtt());
			cityRepository.save(cityToUpdate);
		}else throw new CityNotFoundException("City with ID: " + city.getCityId() + " not found");
	}
	public List<Employee> getAllEmployeesFromCity(long cityId)throws CityNotFoundException{
		if(cityRepository.findById(cityId).isPresent()){
			return employeeRepository.findAllByCityEquals(cityRepository.getOne(cityId));
		}else throw new CityNotFoundException("City with ID: " + cityId + " not found");
	}
}
