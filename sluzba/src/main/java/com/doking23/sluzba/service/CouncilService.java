package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.Council;
import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.exceptions.CouncilNotFoundException;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.repository.CouncilRepository;
import com.doking23.sluzba.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CouncilService {
	private final CouncilRepository councilRepository;
	private final EmployeeRepository employeeRepository;

	public List<Council> getAllCouncils(){
		return councilRepository.findAll();
	}
	public Council getCouncilById(long id){
		if(councilRepository.findById(id).isPresent()){
			return councilRepository.findById(id).get();
		}else throw new CouncilNotFoundException("Council with ID: " + id + " not found");
	}
	public void createCouncil(Council council){
		councilRepository.save(council);
	}
	public void deleteCouncil(long id) throws CouncilNotFoundException{
		if(councilRepository.findById(id).isPresent()){
			councilRepository.deleteById(id);
		}else throw new CouncilNotFoundException("Council with ID: " + id + " not found");
	}
	public void addEmployeeToCouncil(long id,long employeeId)throws CouncilNotFoundException,EmployeeNotFoundException{
		if(councilRepository.findById(id).isPresent()){
			Council councilToAddTo = councilRepository.findById(id).get();
			if(employeeRepository.findById(employeeId).isPresent()){
				Employee employee = employeeRepository.getOne(employeeId);
				councilToAddTo.addEmployee(employee);
				councilRepository.save(councilToAddTo);
			}else throw new EmployeeNotFoundException("Employee with ID: "+employeeId+" not found");
		}else throw new CouncilNotFoundException("Council with ID: "+id+" not found");
	}

}
