package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.School;
import com.doking23.sluzba.exceptions.SchoolNotFoundException;
import com.doking23.sluzba.repository.CityRepository;
import com.doking23.sluzba.repository.EmployeeRepository;
import com.doking23.sluzba.repository.SchoolRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SchoolService {
	private final SchoolRepository schoolRepository;
	private final EmployeeRepository employeeRepository;
	private final CityRepository cityRepository;
	public void createSchool(School school){
		City schoolCity = school.getCity();
		if(cityRepository.findByPttEqualsAndCityNameEquals(schoolCity.getPtt(),schoolCity.getCityName()) == null){
			schoolRepository.save(school);
			schoolCity.addSchool(school);
			cityRepository.save(schoolCity);
		}else {
			City existing = cityRepository.findCityByPttEquals(school.getCity().getPtt());
			school.setCity(existing);
			schoolRepository.save(school);
			existing.addSchool(school);
			cityRepository.save(existing);
		}
	}
	public void deleteSchool(long id){
		schoolRepository.deleteById(id);
	}
	public List<School> getAllSchools(){
		return schoolRepository.findAll();
	}
	public School getSchoolById(long id){
		if(schoolRepository.findById(id).isPresent()){
			return schoolRepository.findById(id).get();
		}else throw new SchoolNotFoundException("School with ID: "+id+" not found");
	}
	public void addEmployeesById(long schoolId,List<Long> employees){
		if(schoolRepository.findById(schoolId).isPresent()){
			employees
					.stream()
					.filter(employee -> employeeRepository
							.findById(employee)
							.isPresent())
					.forEach(employee -> schoolRepository
							.findById(schoolId)
							.get()
							.addEmployee(employeeRepository
									.findById(employee).get()));
		}
	}
}
