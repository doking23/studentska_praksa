package com.doking23.sluzba.service;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Evaluation;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.EvaluationNotFoundException;
import com.doking23.sluzba.repository.EmployeeRepository;
import com.doking23.sluzba.repository.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationService {
	private final EvaluationRepository evaluationRepository;
	private final  EmployeeRepository employeeRepository;
	@Autowired
	public EvaluationService(EvaluationRepository evaluationRepository, EmployeeRepository employeeRepository) {
		this.evaluationRepository = evaluationRepository;
		this.employeeRepository = employeeRepository;
	}
	public List<Evaluation> getAllEvaluations(){
		return evaluationRepository.findAll();
	}
	public void addEvaluation(Evaluation evaluation){
		evaluationRepository.save(evaluation);
	}
	public void deleteEvaluation(long evaluationId) throws EvaluationNotFoundException{
		if(evaluationRepository.findById(evaluationId).isPresent()){
			evaluationRepository.deleteById(evaluationId);
		}else throw new EvaluationNotFoundException("Evaluation with ID: "+evaluationId+" not found");

	}
}
