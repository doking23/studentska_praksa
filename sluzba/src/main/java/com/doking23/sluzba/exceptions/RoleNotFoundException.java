package com.doking23.sluzba.exceptions;


public class RoleNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2203521501087804491L;

	public RoleNotFoundException(String message) {
		super(message);
	}
}
