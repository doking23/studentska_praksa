package com.doking23.sluzba.exceptions;

public class SchoolNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -615152389260283624L;

	public SchoolNotFoundException(String message) {
		super(message);
	}
}
