package com.doking23.sluzba.exceptions;


public class CityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 9211986583876282638L;

	public CityNotFoundException(String message) {
		super(message);
	}
}
