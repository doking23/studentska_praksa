package com.doking23.sluzba.exceptions;


public class CouncilNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5687032019580606770L;

	public CouncilNotFoundException(String message) {
		super(message);
	}
}
