package com.doking23.sluzba.exceptions;

public class TaskNotFoundException extends RuntimeException {


	private static final long serialVersionUID = 2143458054475095916L;

	public TaskNotFoundException(String message) {
		super(message);
	}
}
