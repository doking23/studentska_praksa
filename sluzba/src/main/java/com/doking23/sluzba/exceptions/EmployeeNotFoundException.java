package com.doking23.sluzba.exceptions;

public class EmployeeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 2144700556790952720L;

	public EmployeeNotFoundException(String message) {
		super(message);
	}
}
