package com.doking23.sluzba.exceptions;

public class EvaluationNotFoundException extends RuntimeException {


	private static final long serialVersionUID = 4436248292321323867L;

	public EvaluationNotFoundException(String message) {
		super(message);
	}
}
