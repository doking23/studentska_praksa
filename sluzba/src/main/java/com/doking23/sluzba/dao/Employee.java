package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "employees")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long employeeId;
	@Getter @Setter @JsonProperty
	private String employeeFirstName;
	@Getter @Setter @JsonProperty
	private String employeeLastName;
	@Getter @Setter @JsonProperty
	private String employeePhoneNumber;
	@Getter @Setter @JsonProperty
	private String userName;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "cityId")
	@JsonProperty
	private City city;
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name="employee_role"
			,joinColumns = @JoinColumn(name = "employeeId")
			,inverseJoinColumns = @JoinColumn(name = "roleId"))
	@Getter
	private List<Role> roles = new ArrayList<>();

	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name="employee_task"
			,joinColumns = @JoinColumn(name = "employeeId")
			,inverseJoinColumns = @JoinColumn(name = "taskId"))
	@Getter
	private List<Task> tasks = new ArrayList<>();

	@ManyToOne
	@JoinTable(name = "employee_council"
			,joinColumns =
					{@JoinColumn(name = "employeeId",referencedColumnName = "employeeId")},
			inverseJoinColumns =
					{@JoinColumn(name = "councilId",referencedColumnName = "councilId")})
	@JsonProperty
	@Setter @Getter
	private Council council;
	@ManyToOne
	@JoinColumn(name = "schoolId",nullable = false)
	@JsonProperty
	private School school;
	@OneToOne
	@JoinColumn(
			name = "employee_evaluation",unique = true,updatable = false)
	@JsonProperty
	private Evaluation evaluation;

	public void addRole(Role role){
		if(roles.contains(role)){
			return;
		}
		roles.add(role);
		role.addEmployee(this);
	}
	public void removeRole(Role role){
		if(!roles.contains(role)){
			return;
		}
		roles.remove(role);
		role.removeEmployee(this);
	}
	public void addTask(Task task){
		if(tasks.contains(task)){
			return;
		}
		tasks.add(task);
		task.addEmployee(this);
	}

	public void removeTask(Task task){
		if(!tasks.contains(task))
		tasks.remove(task);
		task.removeEmployee(this);
	}
	public void setEvaluation(Evaluation evaluation){
		if(sameAsFormer(evaluation)){
			return;
		}
		Evaluation oldEvaluation = this.evaluation;
		this.evaluation = evaluation;
		if(oldEvaluation != null){
			oldEvaluation.setEmployee(null);
		}
		if(evaluation !=null){
			evaluation.setEmployee(this);
		}
	}
	public Evaluation getEvaluation(){
		return this.evaluation;
	}
	public void setSchool(School school){
		if(sameAsFormer(school)){
			return;
		}
		School oldSchool = this.school;
		this.school = school;
		if(oldSchool != null){
			oldSchool.removeEmployee(this);
		}
		if(school!=null){
			school.addEmployee(this);
		}
	}
	public School getSchool(){
		return this.school;
	}
	public void setCity(City newCity){
		if(sameAsFormer(newCity)){
			return;
		}
		City oldCity = this.city;
		this.city = newCity;
		if(oldCity!=null){
			oldCity.removeEmployee(this);
		}
		if(newCity!=null){
			newCity.addEmployee(this);
		}
	}
	public City getCity(){
		return this.city;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return employeeId.equals(employee.employeeId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(employeeId);
	}

	private boolean sameAsFormer(Evaluation newEvaluation){
		return Objects.equals(evaluation, newEvaluation);
	}
	private boolean sameAsFormer(City newCity){
		return Objects.equals(city, newCity);
	}
	private boolean sameAsFormer(School newSchool){
		return Objects.equals(school, newSchool);
	}

}
