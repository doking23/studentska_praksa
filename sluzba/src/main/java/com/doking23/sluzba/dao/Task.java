package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "tasks")
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long taskId;
	@Getter @Setter @NaturalId @JsonProperty
	private String taskName;
	@Getter @Setter @JsonProperty
	private String taskType;
	@ManyToMany(mappedBy = "tasks")
	private List<Employee> employees = new ArrayList<>();
	public void addEmployee(Employee employee){
		employees.add(employee);
	}
	public void removeEmployee(Employee employee){
		employees.remove(employee);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Task task = (Task) o;
		return Objects.equals(taskId, task.taskId) &&
				Objects.equals(taskName, task.taskName) &&
				Objects.equals(taskType, task.taskType) &&
				Objects.equals(employees, task.employees);
	}
	@Override
	public int hashCode() {
		return Objects.hash(taskName);
	}
}
