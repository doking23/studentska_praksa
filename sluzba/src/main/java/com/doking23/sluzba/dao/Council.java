package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "councils")
public class Council {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long councilId;
	@Getter @Setter @JsonProperty
	private String councilName;
	@OneToMany
	@JoinTable(name = "employee_council"
			,joinColumns = {@JoinColumn(name = "councilId",referencedColumnName = "councilId")}
			,inverseJoinColumns = {@JoinColumn(name = "employeeId",referencedColumnName = "employeeId")})
	@Getter @Setter
	List<Employee> employees = new ArrayList<>();

	public void addEmployee(Employee employee){
		if(employees.contains(employee)){
			return;
		}
		employees.add(employee);
		employee.setCouncil(this);
	}
	public void removeEmployee(Employee employee){
		if(!employees.contains(employee)){
			return;
		}
		employees.remove(employee);
		employee.setCouncil(null);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Council council = (Council) o;
		return councilId.equals(council.councilId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(councilId);
	}
}
