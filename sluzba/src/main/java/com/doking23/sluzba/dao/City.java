package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "cities")
public class City {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long cityId;
	@Getter @Setter @JsonProperty
	private String cityName;
	@Getter @Setter @JsonProperty
	private Long ptt ;
	@OneToMany(mappedBy = "city",cascade = CascadeType.ALL,orphanRemoval = true)
	private List<School> schools = new ArrayList<>();
	@OneToMany(mappedBy = "city",cascade = CascadeType.ALL,orphanRemoval = true)
	private Set<Employee> employees = new HashSet<>();

	public void addSchool(School school){
		if(schools.contains(school)){
			return;
		}
		schools.add(school);
		school.setCity(this);
	}
	public void removeSchool(School school ){
		if(!schools.contains(school)){
			return;
		}
		schools.remove(school);
		school.setCity(null);
	}
	public void addEmployee(Employee employee){
		if(employees.contains(employee)){
			return;
		}
		employees.add(employee);
		employee.setCity(this);
	}
	public void removeEmployee(Employee employee){
		if(!employees.contains(employee)){
			return;
		}
		employees.remove(employee);
		employee.setCity(null);
	}

}
