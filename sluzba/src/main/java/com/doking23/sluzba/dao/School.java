package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "schools")
public class School {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long schoolId;
	@Getter @Setter @JsonProperty
	private String schoolName;
	@Getter @Setter @JsonProperty
	private double schoolLevel;
	@Getter @Setter @JsonProperty
	private String schoolType;
	@Getter @Setter @JsonProperty
	private String schoolPhoneNumber;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cityId")
	@Nullable @JsonProperty
	private City city;
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "school")
	@Getter
	private Set<Employee> employees = new HashSet<>();

	public void addEmployee(Employee employee){
		if(employees.contains(employee)){
			return;
		}
		employees.add(employee);
		employee.setSchool(this);
	}
	public void removeEmployee(Employee employee){
		employees.remove(employee);
	}
	public void setCity(City city){
		if(sameAsFormer(city)){
			return;
		}
		City oldCity = this.city;
		this.city = city;
		if(oldCity != null){
			oldCity.removeSchool(this);
		}
		if(city!=null){
			city.addSchool(this);
		}
	}
	public City getCity(){
		return this.city;
	}
	private boolean sameAsFormer(City city){
		return Objects.equals(this.city,city);
	}

}
