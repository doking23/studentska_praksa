package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long roleId;
	@Getter @Setter @NaturalId
	@JsonProperty
	private String roleName;
	@Getter @Setter @JsonProperty
	private String roleType;
	@ManyToMany(mappedBy = "roles")
	@Getter
	private List<Employee> employees = new ArrayList<>();

	public void addEmployee(Employee employee){
		employees.add(employee);
	}
	public void removeEmployee(Employee employee){
		employees.remove(employee);
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Role role = (Role) o;
		return roleName.equals(role.roleName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(roleName);
	}
}
