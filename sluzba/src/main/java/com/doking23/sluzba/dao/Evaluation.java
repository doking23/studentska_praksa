package com.doking23.sluzba.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "evaluations")
public class Evaluation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @JsonProperty
	private Long id;
	@Getter @Setter @JsonProperty
	private int evaluationCode;
	@OneToOne(optional = false,mappedBy = "evaluation")
	@JsonProperty
	private Employee employee;
	public void setEmployee(Employee newEmployee){
		if(sameAsBefore(newEmployee)){
			return;
		}
		Employee oldEmployee = this.employee;
		this.employee = newEmployee;
		if(oldEmployee!=null){
			oldEmployee.setEvaluation(null);
		}
		if(employee != null){
			employee.setEvaluation(this);
		}
	}
	private boolean sameAsBefore(Employee newEmployee){
		return Objects.equals(employee,newEmployee);
	}

}
