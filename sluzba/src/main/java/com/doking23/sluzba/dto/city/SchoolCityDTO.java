package com.doking23.sluzba.dto.city;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SchoolCityDTO {
	private long schoolId;
	private String schoolName;
	private String schoolType;
}
