package com.doking23.sluzba.dto.city;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EmployeeCityDTO {
	private long employeeId;
	private String employeeFirstName;
	private String employeeLastName;
}
