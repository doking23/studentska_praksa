package com.doking23.sluzba.dto.employee;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class EmployeeDTO {
	private String employeeFirstName;
	private String employeeLastName;
	private String schoolName;
	private String cityName;
	private List<TaskDTO> tasks;
	private List<RoleDTO> roles;
}
