package com.doking23.sluzba.dto.school;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.Employee;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
public class SchoolDTO {
	@Getter @Setter
	private long schoolId;
	@Getter @Setter @JsonProperty
	private String schoolName;
	@Getter @Setter @JsonProperty
	private double schoolLevel;
	@Getter @Setter @JsonProperty
	private String schoolType;
	@Getter @Setter @JsonProperty
	private String schoolPhoneNumber;
	@Getter @Setter @JsonProperty
	private City city;
}
