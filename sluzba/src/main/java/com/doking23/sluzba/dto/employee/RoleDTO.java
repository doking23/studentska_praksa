package com.doking23.sluzba.dto.employee;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RoleDTO {
	private String roleName;
	private String roleType;
}
