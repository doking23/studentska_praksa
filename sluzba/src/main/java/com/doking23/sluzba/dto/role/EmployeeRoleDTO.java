package com.doking23.sluzba.dto.role;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EmployeeRoleDTO {
	private long employeeId;
	private String firstName;
	private String lastName;
}
