package com.doking23.sluzba.dto.city;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter @Setter
public class CitySchoolsDTO {
	private long cityId;
	private String cityName;
	private List<SchoolCityDTO> schools;
}
