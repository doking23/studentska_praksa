package com.doking23.sluzba.dto.role;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class RoleDTO {
	private long roleId;
	private String roleName;
	private String roleType;
	List<EmployeeRoleDTO> employees;
}
