package com.doking23.sluzba.dto.city;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class CityEmployeesDTO {
	private long cityId;
	private String cityName;
	private List<EmployeeCityDTO> employees;
}
