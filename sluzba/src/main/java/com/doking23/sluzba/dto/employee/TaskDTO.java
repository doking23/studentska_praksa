package com.doking23.sluzba.dto.employee;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TaskDTO {
	private String taskName;
	private String taskType;
}
