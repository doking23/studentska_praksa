package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dto.city.CityEmployeesDTO;
import com.doking23.sluzba.dto.city.CitySchoolsDTO;
import com.doking23.sluzba.dto.city.EmployeeCityDTO;
import com.doking23.sluzba.service.CityService;
import com.doking23.sluzba.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "city")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CityController {
	private final CityService cityService;
	private final EmployeeService employeeService;
	private final ModelMapper modelMapper;

//	@GetMapping
//	@ResponseBody
//	public List<City> getAllCities(){
//		return cityService.getAllCities();
//	}
	@PostMapping
	public void createCity(@RequestBody City city){
		cityService.createCity(city);
	}
	@PutMapping(path = "{id}")
	public void updateCity(@PathVariable long id,@RequestBody City city){
		cityService.updateCity(id,city);
	}
	@DeleteMapping(path = "{id}")
	public void deleteCity(@PathVariable long id){
		cityService.deleteCity(id);
	}
//	@GetMapping(path = "employees")
//	@ResponseBody
//	public List<CityEmployeesDTO> getAllEmployeesFromCity(){
//		Function<City,CityEmployeesDTO> convertToDTO = (City city) ->{
//			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
//			return modelMapper.map(city, CityEmployeesDTO.class);
//		};
//		return cityService.getAllCities()
//				.stream()
//				.map(convertToDTO)
//				.collect(Collectors.toList());
//	}

	@GetMapping(path = "{id}/employees")
	@ResponseBody
	public List<EmployeeCityDTO> getAllSchoolsFromCity(@PathVariable long id){
		Function<Employee,EmployeeCityDTO> convertToDTO = (Employee employee) ->{
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(employee,EmployeeCityDTO.class);
		};
		return cityService.getAllEmployeesFromCity(id)
				.stream()
				.map(convertToDTO)
				.collect(Collectors.toList());
	}

}


