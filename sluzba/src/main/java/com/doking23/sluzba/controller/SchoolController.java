package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.School;
import com.doking23.sluzba.dto.school.SchoolDTO;
import com.doking23.sluzba.service.SchoolService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping(path = "school")
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SchoolController {
	private final SchoolService schoolService;
	private final ModelMapper modelMapper;
	@GetMapping
	public List<SchoolDTO> getAllSchools(){
		Function<School,SchoolDTO> convertSchoolToDTO = school -> {
			modelMapper.getConfiguration()
					.setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(school,SchoolDTO.class);
		};
		return schoolService.getAllSchools()
				.stream()
				.map(convertSchoolToDTO)
				.collect(Collectors.toList());
	}
	@PostMapping
	public void createSchool(@RequestBody School school){
		schoolService.createSchool(school);
	}
	@GetMapping(path = "{id}")
	@ResponseBody
	public SchoolDTO getSchoolById(@PathVariable long id){
		Function<School,SchoolDTO> convertSchoolToDTO = school -> {
			modelMapper.getConfiguration()
					.setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(school,SchoolDTO.class);
		};
		return convertSchoolToDTO.apply(schoolService.getSchoolById(id));
	}
	@DeleteMapping(path = "{id}")
	public void deleteSchoolById(@PathVariable long id){
		schoolService.deleteSchool(id);
	}
}
