package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.*;
import com.doking23.sluzba.dto.employee.EmployeeDTO;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("/employee")
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeController {
	private final EmployeeService employeeService;
	private final ModelMapper modelMapper;
	@GetMapping(path = "/all")
	@ResponseBody
	public List<EmployeeDTO> getAllEmployees(){
		Function<Employee,EmployeeDTO> convertToDTO = (Employee employee) ->{
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(employee,EmployeeDTO.class);
		};

		return employeeService.getAllEmployees()
				.stream()
				.map(convertToDTO)
				.collect(Collectors.toList());
	}
	@GetMapping(path = "{id}")
	@ResponseBody
	public EmployeeDTO getEmployeeById(@PathVariable long id){
		Function<Employee,EmployeeDTO> convertToDTO = (Employee employee) ->{
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(employee,EmployeeDTO.class);
		};

		return convertToDTO.apply(employeeService.getEmployeeById(id));
	}
	@PostMapping
	public void createEmployee(@RequestBody Employee employee){
		employeeService.createEmployee(employee);
	}
	@DeleteMapping(path = "{employeeId}")
	public void deleteEmployee(@PathVariable long employeeId){
		employeeService.deleteEmployee(employeeId);
	}
	@PutMapping(path = "/city/{employeeId}")
	public void changeEmployeeCity(@PathVariable long employeeId, @RequestBody City city){
		employeeService.changeCity(employeeId,city);
	}
	@PutMapping(path = "/evaluate/{employeeId}")
	public void evaluateEmployee(@PathVariable long employeeId, @RequestBody Evaluation evaluation){
		employeeService.evaluateEmployee(employeeId,evaluation);
	}
	@PutMapping(path = "/roles/{employeeId}")
	public void addRoleToEmployee(@PathVariable long employeeId, @RequestBody List<Role> roles){
		employeeService.addRolesToEmployee(employeeId,roles);
	}
	@PutMapping(path = "/tasks/{employeeId}")
	public void addTaskToEmployee(@PathVariable long employeeId, @RequestBody List<Task> tasks){
		employeeService.addTasksToEmployee(employeeId,tasks);
	}
	@DeleteMapping(path = "/evaluation/{employeeId}/{evaluationId}")
	public void deleteEvaluation(@PathVariable long employeeId,@PathVariable long evaluationId){
		employeeService.removeEvaluation(employeeId,evaluationId);
	}
//	@DeleteMapping(path = "/roles/{employeeId}")
//	public void deleteRole(@PathVariable long employeeId,@RequestBody List<String> roles){
//		employeeService.removeRolesEmployee(employeeId,roles);
//	}
//	@DeleteMapping(path = "/tasks/{employeeId}")
//	public void deleteTask(@PathVariable long employeeId,@PathVariable @RequestBody List<String> tasks){
//		employeeService.removeTasksEmployee(employeeId,tasks);
//	}
	@ExceptionHandler(EmployeeNotFoundException.class)
	public void handleEmployeeNotFoundException(EmployeeNotFoundException exception, HttpServletResponse response) throws IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}
