package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.Task;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.TaskNotFoundException;
import com.doking23.sluzba.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RequestMapping(path = "task")
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TaskController {
	private final TaskService taskService;
	@GetMapping
	@ResponseBody
	public List<Task> getAllTasks(){
		return taskService.getAllTasks();
	}
	@PostMapping
	public void createTask(@RequestBody Task task){
		taskService.createTask(task);
	}
	@PutMapping(path = "/{employeeId}/{taskId}")
	public void addTaskToEmployee(@PathVariable long employeeId,@PathVariable long taskId){
		taskService.addTaskToEmployee(employeeId,taskId);
	}
	@DeleteMapping(path = "/{employeeId}/{taskId}")
	public void removeTaskFromEmployee(@PathVariable long employeeId,@PathVariable long taskId){
		taskService.removeTaskFromEmployee(taskId,employeeId);
	}
	@ExceptionHandler(TaskNotFoundException.class)
	public void handleTaskNotFoundException(TaskNotFoundException exception, HttpServletResponse response) throws IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	@ExceptionHandler(EmployeeNotFoundException.class)
	public void handleTaskNotFoundException(EmployeeNotFoundException exception, HttpServletResponse response) throws IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}
