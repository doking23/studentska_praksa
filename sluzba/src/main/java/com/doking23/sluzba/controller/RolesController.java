package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.Role;
import com.doking23.sluzba.dto.role.RoleDTO;
import com.doking23.sluzba.exceptions.EmployeeNotFoundException;
import com.doking23.sluzba.exceptions.RoleNotFoundException;
import com.doking23.sluzba.service.RoleService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("role")
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RolesController {

	private final RoleService roleService;
	private final ModelMapper modelMapper;
	@GetMapping(path = "{id}")
	@ResponseBody
	public RoleDTO getRoleBuId(@PathVariable long id){
		Function<Role,RoleDTO> convertToDTO = (Role role)->{
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(role,RoleDTO.class);
		};
		return convertToDTO.apply(roleService.getRoleById(id));
	}
	@GetMapping
	@ResponseBody
	public List<RoleDTO> getAllRoles(){
		Function<Role,RoleDTO> convertToDTO = (Role role)->{
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
			return modelMapper.map(role,RoleDTO.class);
		};
		return roleService.getAllRoles()
				.stream()
				.map(convertToDTO)
				.collect(Collectors.toList());
	}
	@PostMapping
	public void addRole(@RequestBody Role role){
		roleService.addRole(role);
	}
	@PutMapping(path = "{id}")
	public void updateRole(@PathVariable long id,@RequestBody Role role){
		roleService.updateRole(id,role);
	}
	@DeleteMapping(path = "{id}")
	public void deleteRole(@PathVariable long id){
		roleService.deleteRole(id);
	}
	@PutMapping(path = "/{employee}/{role}")
	public void addRoleToEmployee(@PathVariable long employee,@PathVariable long role){
		roleService.addRoleToEmployee(employee,role);
	}
	@DeleteMapping(path = "/{employeeId}/{roleId}")
	public void removeRoleFromEmployee(@PathVariable long employeeId,@PathVariable long roleId){
		roleService.removeRoleFromEmployee(employeeId,roleId);
	}
	@ExceptionHandler(RoleNotFoundException.class)
	public void handleRoleNotFoundException(RoleNotFoundException exception, HttpServletResponse response) throws IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND,exception.getMessage());
	}
	@ExceptionHandler(EmployeeNotFoundException.class)
	public void handleEmployeeNotFoundException(EmployeeNotFoundException exception,HttpServletResponse response) throws IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND,"Error employee not found");
	}

}
