package com.doking23.sluzba.controller;

import com.doking23.sluzba.dao.Council;
import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.service.CouncilService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "council")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CouncilController {
	private final CouncilService councilService;

	@GetMapping
	@ResponseBody
	public List<Council> getAllCouncils(){
		return councilService.getAllCouncils();
	}
	@PostMapping
	public void createCouncil(@RequestBody Council council) {
		councilService.createCouncil(council);
	}
	@GetMapping(path = "{id}")
	@ResponseBody
	public Council getCouncilById(@PathVariable long id ){
		return councilService.getCouncilById(id);
	}
	@DeleteMapping(path = "{id}")
	public void deleteCouncil(@PathVariable long id){
		councilService.deleteCouncil(id);
	}
	@PutMapping(path = "{councilId}/{employeeId}")
	public void addEmployeeToCouncil(@PathVariable long councilId, @PathVariable long employeeId ){
		councilService.addEmployeeToCouncil(councilId,employeeId);
	}
}
