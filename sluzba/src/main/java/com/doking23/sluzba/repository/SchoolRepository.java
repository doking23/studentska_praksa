package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolRepository extends JpaRepository<School,Long> {
	boolean existsBySchoolNameEqualsAndSchoolTypeEquals(String name,String type);
	School findSchoolBySchoolNameEqualsAndSchoolTypeEquals(String name,String type);
}
