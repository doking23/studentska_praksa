package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City,Long> {
	List<Employee> findAllByCityNameEquals(String cityName);;
	City findByPttEqualsAndCityNameEquals(Long ptt,String name);
	City findCityByPttEquals(Long ptt);


}
