package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
//	Role findRoleByRoleNameAndEmployee(String name,Employee employee);
//	boolean existsByEAndEmployee_EmployeeId(long employeeId);
}
