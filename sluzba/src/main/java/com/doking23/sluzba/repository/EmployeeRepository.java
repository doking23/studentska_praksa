package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.City;
import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
	Employee getEmployeeByEmployeeIdAndEvaluationEquals(long id,Evaluation evaluation);
	List<Employee> findAllByCityEquals(City city);
}
