package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.Council;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CouncilRepository extends JpaRepository<Council,Long> {
}
