package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {

}
