package com.doking23.sluzba.repository;

import com.doking23.sluzba.dao.Employee;
import com.doking23.sluzba.dao.Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation,Long> {
}
